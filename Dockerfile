FROM ubuntu:latest

MAINTAINER Eoin O'Neill <eoinoneill1991@gmail.com>
MAINTAINER Emmet O'Neill <emmetoneill.pdx@gmail.com>

# Setup mirrors..
RUN sed -E -i 's#http://us.archive\.ubuntu\.com/ubuntu#mirror://mirrors.ubuntu.com/mirrors.txt#g' /etc/apt/sources.list && \
    sed -E -i 's#http://us.security\.ubuntu\.com/ubuntu#mirror://mirrors.ubuntu.com/mirrors.txt#g' /etc/apt/sources.list

# Avoid tzdata configuration popups..
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y tzdata

#=========================#

# Update System
RUN apt-get update

# Install General System Utilities..
RUN apt-get install -y \
    gpg \
    sysvinit-utils \
    vim \
    nano \
    git \
    subversion \
    curl \
    wget \
    gettext \
    locales \
    locate \
    build-essential \
    cmake \
    extra-cmake-modules \
    gdb \
    gperf \
    valgrind \
    fuse \
    rsync \
    patchelf \
    libtool 

# # === Deps of Deps ===

RUN apt-get install -y \ 
    zlib1g-dev \
    # note: might not need zlib since we're building it in ext_zlib?
    libxcb-util0-dev \
    libxcb-res0-dev	\

# Install Qt Dependencies
# https://doc.qt.io/qt-5/linux-requirements.html
RUN apt-get install -y \
    libx11-dev libx11-xcb-dev libxext-dev libxfixes-dev libxi-dev \
    libxrender-dev libxcb1-dev libxcb-glx0-dev libxcb-keysyms1-dev \
    libxcb-image0-dev libxcb-shm0-dev libxcb-icccm4-dev libxcb-sync-dev \
    libxcb-xfixes0-dev libxcb-shape0-dev libxcb-randr0-dev libxcb-render-util0-dev \
    libxcb-xinerama0-dev libxkbcommon-dev libxkbcommon-x11-dev \
    libfontconfig1-dev libfreetype6-dev mesa-common-dev libgl1-mesa-dev libegl1-mesa-dev mesa-utils 

# # Install KDEFrameworks Deps..
# # NOTE: It's better to build these from Krita's cmake `ext_frameworks`.
# RUN apt-get install -y \
#     libkf5config-dev libkf5widgetsaddons-dev libkf5completion-dev \
#     libkf5coreaddons-dev libkf5guiaddons-dev libkf5i18n-dev libkf5itemmodels-dev \
#     libkf5itemviews-dev libkf5windowsystem-dev

# # === Install Core Krita Dependencies.. ===
# # NOTE: It's better to build these from Krita's 3rdparty cmake.
# RUN apt-get install -y \
#     libpq-dev libaio-dev libasound2-dev libatkmm-1.6-dev libbz2-dev libcairo-perl \ 
#     libcap-dev libcups2-dev libdbus-1-dev libdrm-dev libegl1-mesa-dev \
#     libgcrypt20-dev libgl1-mesa-dev libglib-perl libgsl0-dev libgsl0-dev \
#     gstreamer1.0-alsa libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev \
#     libgtk3-perl libjpeg-dev libnss3-dev libpci-dev libpng-dev libpulse-dev \
#     libssl-dev libgstreamer-plugins-good1.0-dev libgstreamer-plugins-bad1.0-dev \
#     gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-ugly \
#     libtiff5-dev libudev-dev libwebp-dev flex libmysqlclient-dev \
#     libxcomposite-dev libxcursor-dev libxdamage-dev \
#     libxrandr-dev libxss-dev libxtst-dev mesa-common-dev libffi-dev liblist-moreutils-perl \
#     libpixman-1-dev python3-dev libicu-dev libqt5svg5-dev libqt5x11extras5-dev

# TODO: Install Krita Documentation Dependencies..
# RUN apt-get install -y python3-sphinx

# TODO: Install Krita Website & DevFund Dependencies..
# RUN apt-get install -y libmariadb-dev

#========================#

# Ubuntu 22.04 workaround for libssl1.1, needed by cmake-extra-modules.
#RUN echo "deb http://security.ubuntu.com/ubuntu focal-security main" | tee /etc/apt/sources.list.d/focal-security.list

#RUN apt-get update
#RUN apt-get install libssl1.1

#========================#

# Manually update CMake
#RUN wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null | gpg --dearmor - | tee /usr/share/keyrings/kitware-archive-keyring.gpg >/dev/null
#RUN echo 'deb [signed-by=/usr/share/keyrings/kitware-archive-keyring.gpg] https://apt.kitware.com/ubuntu/ bionic main' | tee /etc/apt/sources.list.d/kitware.list >/dev/null
#RUN apt-get update && apt-get install -y kitware-archive-keyring 
#RUN apt-get update && apt-get install -y cmake extra-cmake-modules

# Manually update GCC/G++
#RUN apt-get install -y software-properties-common
#RUN add-apt-repository ppa:ubuntu-toolchain-r/test
#RUN apt-get update && apt-get install -y gcc-11 g++-11 

#======================#

# Setup Krita-Devbox Environment & Utilities..
# RUN apt-get install -y python3-click

## Set Krita-specific Environment Variables..
ENV KRITA_DIR=/var/krita

## Make the dirs..
RUN mkdir -p ${KRITA_DIR}/source
RUN mkdir -p ${KRITA_DIR}/build
RUN mkdir -p ${KRITA_DIR}/install
RUN mkdir -p ${KRITA_DIR}/deps
RUN mkdir -p ${KRITA_DIR}/deps/download
RUN mkdir -p ${KRITA_DIR}/deps/build
RUN mkdir -p ${KRITA_DIR}/deps/install

RUN chmod -R 757 ${KRITA_DIR}

## Install custom scripts and configurations..
COPY ./bin/krita_env.py \
     ./bin/krita-build-appimage \
     ./bin/krita-build-dependencies \  
     ./bin/krita-cmake \
     ./bin/krita-devbox-setup \
     ./bin/krita-devbox \
     /bin/

COPY ./bin/krita-dev-environment.inc \
     /opt/

#======================#

# TODO: Re-Setup AppImage Deployment..
#RUN wget -c -nv "https://github.com/probonopd/linuxdeployqt/releases/download/continuous/linuxdeployqt-continuous-x86_64.AppImage" -O /bin/linuxdeployqt
#RUN chmod a+x /bin/linuxdeployqt

## Setup appimage build symlinks.
#RUN ln -s ${KRITA_DIR}/appdir ${KRITA_DIR}/krita.appdir
#RUN ln -s ${KRITA_DIR}/build ${KRITA_DIR}/krita-build

# TODO: Setup Flatpak Deployment..

## HACK: These need to be assigned to get Click to work as a distrobox init-hook
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

RUN /bin/bash -c "source /opt/krita-dev-environment.inc"
