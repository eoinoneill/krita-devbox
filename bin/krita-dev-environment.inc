prepend() { eval $1=\"$2\$\{$1:+':'\$$1\}\" && export $1 ; }

prepend PATH $KRITA_INSTALL_DIR/bin
prepend PATH $KRITA_DEPS_DIR/bin

prepend LD_LIBRARY_PATH $KRITA_INSTALL_DIR/lib64
prepend LD_LIBRARY_PATH $KRITA_INSTALL_DIR/lib
prepend LD_LIBRARY_PATH $KRITA_DEPS_DIR/lib64
prepend LD_LIBRARY_PATH $KRITA_DEPS_DIR/lib
prepend LD_LIBRARY_PATH $KRITA_DEPS_DIR/lib/x86_64-linux-gnu

prepend PKG_CONFIG_PATH $KRITA_DEPS_DIR/lib64/pkgconfig
prepend PKG_CONFIG_PATH $KRITA_DEPS_DIR/lib/pkgconfig
prepend PKG_CONFIG_PATH $KRITA_INSTALL_DIR/lib64/pkgconfig
prepend PKG_CONFIG_PATH $KRITA_INSTALL_DIR/lib/pkgconfig

prepend CMAKE_PREFIX_PATH $KRITA_DEPS_DIR

prepend PYTHONPATH $KRITA_DEPS_DIR/sip

# Disable leak checks in case ASAN is used.
# It takes a lot of time to finalize the leaks 
# and we usually just check for invalid access
export ASAN_OPTIONS=detect_leaks=0

